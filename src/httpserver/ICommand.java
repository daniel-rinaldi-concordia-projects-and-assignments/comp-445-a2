package httpserver;

public interface ICommand
{
	void reset();
	void execute();
}
