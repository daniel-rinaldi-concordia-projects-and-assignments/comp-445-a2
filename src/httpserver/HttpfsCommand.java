package httpserver;

import java.io.IOException;
import java.util.HashMap;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(commandNames = "httpfs", commandDescription = "httpfs is a simple file server.")
public class HttpfsCommand implements ICommand
{
	private JCommander commander;
	
	@Parameter(names = "help", help = true, description = "Prints this usage text.")
	private boolean help;
	
	@Parameter(names = "lsport", description = "List all ports currently in use.")
	private boolean lsport;
	
	@Parameter(names = "start", description = "Starts the server.")
	private boolean start;
	
	@Parameter(names = "-v", description = "Prints debugging messages.")
    private boolean debug;

    @Parameter(names = "-p", description = "Specifies the port number that the server will listen and serve at. Default is 8080.")
    private int port;

    @Parameter(names = "-d", description = "Specifies the path to the directory that the server will use to read/write requested files. Default is {current directory}/data/www/ when launching the application.")
    private String directory;
	
    @Parameter(names = "stop", description = "Stop listening on the specified port.")
    private int stopPort;
	
    private HashMap<Integer, HttpServer> activeServers;
    
	public HttpfsCommand()
	{
		activeServers = new HashMap<Integer, HttpServer>();
		reset();
	}
	
	@Override
	public void reset()
	{
		help = false;
		lsport = false;
		start = false;
		port = 8080;
		debug = false;
		directory = null;
		stopPort = -1;
	}
	
	@Override
	public void execute()
	{
		if (commander == null) return;
		
		if (stopPort >= 0)
		{
			if (activeServers.containsKey(stopPort))
			{
				try
				{
					activeServers.get(stopPort).kill();
					activeServers.remove(stopPort);
				}
				catch (IOException e) 
				{
					if (debug)
						System.err.println("There was a problem closing the server at port " + stopPort + ": " + e.getMessage());
				}
			}
		}
		
		if (lsport)
		{
			if (activeServers.isEmpty())
			{
				System.out.println("==== HTTPFS IS NOT LISTENING ON ANY PORTS ====");
			}
			else
			{
				System.out.println("======================");
				for (HttpServer server : activeServers.values())
				{
					System.out.println("= " + server.getLocalPort());
				}
				System.out.println("======================");
			}
		}
		
		if (help)
		{
			commander.usage();
		}
		else if (start)
		{
	        try
	        {
	        	port = port < 0 ? 8080 : port;
	        	if (!activeServers.containsKey(port))
	        	{
	        		HttpServer server = new HttpServer(directory, port, debug);
	        		activeServers.put(port, server);
	        		new Thread(server).start();
	        	}
	        	else
	        	{
	        		System.out.println("Server is already running on this port.");
	        	}
	        }
	        catch(Exception e) 
	        {
	        	System.err.println(e.getMessage());
	        	e.printStackTrace();
	        }
		}
	}

	public boolean isHelp()
	{
		return help;
	}

	public void setHelp(boolean help)
	{
		this.help = help;
	}

	public JCommander getCommander()
	{
		return commander;
	}

	public void setCommander(JCommander commander)
	{
		this.commander = commander;
	}

	public boolean isDebug() 
	{
		return debug;
	}

	public void setDebug(boolean debug) 
	{
		this.debug = debug;
	}

	public int getPort() 
	{
		return port;
	}

	public void setPort(int port) 
	{
		this.port = port;
	}

	public String getDirectory() 
	{
		return directory;
	}

	public void setDirectory(String directory) 
	{
		this.directory = directory;
	}

	public boolean isStart() 
	{
		return start;
	}

	public void setStart(boolean start) 
	{
		this.start = start;
	}

	public boolean getLSPort() 
	{
		return lsport;
	}

	public void setLSPort(boolean lsport) 
	{
		this.lsport = lsport;
	}

	public int getStopPort() 
	{
		return stopPort;
	}

	public void setStopPort(int stopPort) 
	{
		this.stopPort = stopPort;
	}
	
	public void killAll()
	{
		for (HttpServer server : activeServers.values())
		{
			try 
			{
				server.kill();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
}
