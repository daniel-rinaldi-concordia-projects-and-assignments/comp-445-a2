package httpserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.FileNameMap;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class HttpServer implements Runnable
{
	private static final String DEFAULT_DIRECTORY = "data/www/";
	private static final int DEFAULT_PORT = 8080;
	
	private ServerSocket serverSocket;
	private File workingDirectory;
	private boolean debug;
	private boolean keepListening = true;
	
	private static final FileNameMap MIME_MAP = URLConnection.getFileNameMap();
	
	public HttpServer() throws IOException
	{
		this(null, -1);
	}
	public HttpServer(String directory, int port) throws IOException
	{
		this(directory, port, false);
	}
	public HttpServer(String directory, int port, boolean debug) throws IOException
	{
		port = port < 0 ? DEFAULT_PORT : port;
		this.serverSocket = new ServerSocket(port);
		this.debug = debug;
		
		workingDirectory = directory == null || directory.isBlank() ? new File(DEFAULT_DIRECTORY) : new File(directory);
		if (!workingDirectory.exists() || !workingDirectory.isDirectory())
			workingDirectory = new File(DEFAULT_DIRECTORY);
	}
	
	@Override
	public void run() 
	{
		logInfoln(
			"Server started on port " + serverSocket.getLocalPort() + "\n" +
			"\tport: " + serverSocket.getLocalPort() + "\n" +
			"\tworkingDirectory: " + workingDirectory.getAbsolutePath()
		);
		
		System.out.println("Listening on port " + serverSocket.getLocalPort() + " ...");
		while (keepListening)
			listen();
	}
	
	public void listen()
	{
		PrintWriter responseWriter = null;
		try
		{
			Socket clientSocket = serverSocket.accept(); // serverSocket.accept() blocks until a connection is made
			
			if (clientSocket != null)
			{
				responseWriter = new PrintWriter(clientSocket.getOutputStream());
				BufferedReader requestReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				
				String line1 = requestReader.readLine();
				if (line1 == null) return;
				String[] line1Split = line1.split("\\s");
				String method = line1Split.length > 0 ? line1Split[0] : "";
				String path = line1Split.length > 1 ? line1Split[1] : "";
				path = workingDirectory.getPath().replaceAll("\\\\\\\\|\\\\", "/") + path;
				//String protocolStr = line1Split.length > 2 ? line1Split[2] : null;
				
				// separate queryString from path
				String queryString = "";
				if (path.contains("?"))
				{
					queryString = path.substring(path.indexOf("?")+1);
					path = path.substring(0, path.indexOf("?"));
				}
				
				if ("GET".equalsIgnoreCase(method))
				{
					doGet(clientSocket, path, queryString);
				}
				else if ("POST".equalsIgnoreCase(method))
				{
					String body = "";
					String bodyLine = "";
					int contentLength = 0;
					boolean invalidFormat = false;
					while((bodyLine = requestReader.readLine()) != null) 
					{
						if (bodyLine.isBlank())
							break;
						
						String[] kv = bodyLine.split(":");
						if (kv.length > 1)
						{
							try {contentLength = Integer.valueOf(kv[1].trim());} catch (NumberFormatException e) { invalidFormat = true; }
						}
		            }
					for (int i = 0; i < contentLength; ++i)
					{
						body += Character.toString(requestReader.read());
					}
					
					doPost(clientSocket, path, queryString, body);
				}
				else
				{
					logErrorln("Recieved invalid or unsupported request method: `" + method + "`");
					responseWriter.print("HTTP/1.0 400 Bad Request\r\n");
					responseWriter.print("Server: HTTPFS\r\n");
					responseWriter.print("Connection: close\r\n");
			        responseWriter.flush();
					responseWriter.close();
				}
				
				//requestReader.close();
			}
		}
		catch (SocketException e)
		{
			if (serverSocket.isClosed() && keepListening)
				logErrorln("Server socket is closed. " + e.getMessage());
		}
		catch (Exception e)
		{
			logErrorln(e.getMessage());
			responseWriter.print("HTTP/1.0 500 ServerError\r\n");
			responseWriter.print("Server: HTTPFS\r\n");
			responseWriter.print("Connection: close\r\n");
			responseWriter.print("\r\n");
	        responseWriter.flush();
		}
		finally
		{
			if (responseWriter != null) responseWriter.close();
		}
	}
	
	private void doGet(Socket clientSocket, String path, String queryString) throws IOException
	{
		PrintWriter responseWriter = new PrintWriter(clientSocket.getOutputStream());
        
		Map<String, String> queryStrMap = new HashMap<String, String>();
		if (!queryString.isBlank())
		{
			String[] queryStrArr = queryString.split("&");
			for (int i = 0; i < queryStrArr.length; ++i)
			{
				String[] kv = queryStrArr[i].split("=");
				queryStrMap.put(kv[0], kv[1]);
			}
		}
		
		if (path.isBlank())
		{
			// get all files and folders in workingDirectory
            String responseBody = buildFileListResponse(workingDirectory);
            responseWriter.print("HTTP/1.0 200 OK\r\n");
            responseWriter.print("Server: HTTPFS\r\n");
            responseWriter.print("Content-Type: " + "text/plain" + "\r\n");
            responseWriter.print("Content-Length: " + responseBody.length() + "\r\n");
            responseWriter.print("Connection: close\r\n");
            responseWriter.print("\r\n");
            responseWriter.print(responseBody + "\r\n");
            responseWriter.flush();
		}
		else
		{
			// get the file or all files and folders in the directory specified by the path
			File file = new File(path);
			if (!isPathAllowed(path))
			{
				responseWriter.print("HTTP/1.0 403 Forbidden\r\n");
				responseWriter.print("Server: HTTPFS\r\n");
				responseWriter.print("Connection: close\r\n");
				responseWriter.print("\r\n");
		        responseWriter.flush();
			}
			else if (!file.exists())
			{
				responseWriter.print("HTTP/1.0 404 Not Found\r\n");
				responseWriter.print("Server: HTTPFS\r\n");
				responseWriter.print("Connection: close\r\n");
				responseWriter.print("\r\n");
		        responseWriter.flush();
			}
			else
			{
				responseWriter.print("HTTP/1.0 200 OK\r\n");
                responseWriter.print("Server: HTTPFS\r\n");
                
                if (file.isFile())
                {
                	BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                    StringBuilder sbFileContents = new StringBuilder();
                    String line = "";
                    while ((line = bufferedReader.readLine()) != null) 
                    {
                    	sbFileContents.append(line);
                    	sbFileContents.append("\r\n");
                    }
                    bufferedReader.close();
                    
                    String[] splitPath = path.split("/");
                    String filename = splitPath[splitPath.length - 1];
                    
                    String contentType = MIME_MAP.getContentTypeFor(filename);
                	
	                responseWriter.print("Content-Type: " + contentType+ "\r\n");
	                if (!queryStrMap.containsKey("display") || (queryStrMap.containsKey("display") && !"txt".equalsIgnoreCase(queryStrMap.get("display"))))
	                	responseWriter.print("Content-Disposition: attachment; filename=\"" + filename + "\"\r\n");
	                responseWriter.print("Content-Length: " + file.length() + "\r\n");
	                responseWriter.print("Connection: close\r\n");
	                responseWriter.print("\r\n");
	                responseWriter.print(sbFileContents.length() == 0 ? "\r\n" : sbFileContents.toString() + "\r\n");
                }
                else if (file.isDirectory())
                {
                	String responseBody = buildFileListResponse(file);
                    responseWriter.print("Content-Type: " + "text/plain" + "\r\n");
                    responseWriter.print("Content-Length: " + responseBody.length() + "\r\n");
                    responseWriter.print("Connection: close\r\n");
                    responseWriter.print("\r\n");
                    responseWriter.print(responseBody + "\r\n");
                }
                
                responseWriter.flush();
			}
		}
		
        responseWriter.close();
	}
	
	private void doPost(Socket clientSocket, String path, String queryString, String body) throws IOException
	{
		PrintWriter responseWriter = new PrintWriter(clientSocket.getOutputStream());
		
		Map<String, String> queryStrMap = new HashMap<String, String>();
		if (!queryString.isBlank())
		{
			String[] queryStrArr = queryString.split("&");
			for (int i = 0; i < queryStrArr.length; ++i)
			{
				String[] kv = queryStrArr[i].split("=");
				queryStrMap.put(kv[0], kv[1]);
			}
		}
		
		if (!isPathAllowed(path))
		{
			responseWriter.print("HTTP/1.0 403 Forbidden\r\n");
			responseWriter.print("Server: HTTPFS\r\n");
			responseWriter.print("Connection: close\r\n");
			responseWriter.print("\r\n");
	        responseWriter.flush();
		}
		else
		{
			PrintWriter printWriter = new PrintWriter(new FileOutputStream(path, false));
			printWriter.print(body);
			printWriter.flush();
			printWriter.close();
			
			responseWriter.print("HTTP/1.0 201 Created\r\n");
            responseWriter.print("Server: HTTPFS\r\n");
            responseWriter.print("Connection: close\r\n");
            responseWriter.print("\r\n");
            responseWriter.flush();
		}
		
		responseWriter.close();
	}
	
	private String buildFileListResponse(File dir) 
	{
        File[] files = dir.listFiles();
		
		StringBuilder sb = new StringBuilder();
        for (int i = 0; i < files.length; ++i)
        {
            sb.append(files[i].getName());
            
            if (files[i].isDirectory())
            	sb.append("/");
            
            sb.append("\r\n");
        }
        
        return sb.toString();
	}
	
	private boolean isPathAllowed(String path) 
	{
		// logic : 
		// if you use `../` more than you use `/`, then you are trying to access a 
		// file/folder higher than the workingDirectory which is not allowed
		
		path = path.replaceFirst(workingDirectory.getPath().replace('\\', '/'), "");
		int levels = path.startsWith("/") ? -1 : 0;
		
        Matcher regularSlashMatcher = Pattern.compile("(?<!\\.\\.)\\/").matcher(path);
        while (regularSlashMatcher.find())
            ++levels;
        
        Matcher dotdotSlashMatcher = Pattern.compile("(?<=\\.\\.)\\/").matcher(path);
        while (dotdotSlashMatcher.find())
            --levels;
		
		if (levels < 0) return false;
		
		return true;
	}
	
	private void logInfoln(String debugMsg)
	{
		logInfo(debugMsg+"\r\n");
	}
	private void logInfo(String debugMsg)
	{
		if (debug)
			System.out.print("[" + serverSocket.getLocalPort() + "] INFO: " + debugMsg);
	}
	
	private void logErrorln(String debugMsg)
	{
		logError(debugMsg+"\r\n");
	}
	private void logError(String debugMsg)
	{
		if (debug)
			System.err.print("[" + serverSocket.getLocalPort() + "] ERROR: " + debugMsg);
	}
	
	public int getLocalPort()
	{
		return serverSocket.getLocalPort();
	}
	
	public void kill() throws IOException
	{
		this.keepListening = false;
		serverSocket.close();
	}
}
