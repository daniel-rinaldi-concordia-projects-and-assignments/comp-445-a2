package httpserver;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

public class Httpfs 
{	
	public static void main(String[] args)
	{
		Httpfs httpfs = new Httpfs();
		
		if (args != null && args.length > 0)
			httpfs.cmdStr = String.join(" ", args);
		
		httpfs.start();
	}
	
	private Scanner scanner;
	private String cmdStr;
	private HttpfsCommand httpfsCmd;
	private QuitCommand quitCmd;
	private JCommander jCommander;
	
	public Httpfs()
	{
		scanner = new Scanner(System.in);
		
		quitCmd = new QuitCommand();
		httpfsCmd = new HttpfsCommand();
		
		jCommander = JCommander.newBuilder()
				.addCommand(quitCmd)
				.addCommand(httpfsCmd)
				.build();
		
		jCommander.setUsageFormatter(new CommandUsageFormatter(jCommander));
		httpfsCmd.setCommander(jCommander);
	}
	
	public void start()
	{
		if (cmdStr == null)
			jCommander.usage();
		
		loop();
	}
	
	private void loop()
	{
		while (!"quit".equals(jCommander.getParsedCommand()))
		{
			try
			{
				ResetAllCommands();
				
				if (cmdStr == null)
				{
					if (scanner.hasNext() && scanner.hasNextLine())
						cmdStr = scanner.nextLine();
				}
				
				if (cmdStr != null && !cmdStr.trim().isEmpty())
				{
					// parse command tokens
					jCommander.parse(splitCommandLineInput(cmdStr));
					
					if ("httpfs".equals(jCommander.getParsedCommand()))
						httpfsCmd.execute();
				}
			}
			catch (ParameterException e) 
			{
				  System.err.println("Error: " + e.getMessage());
				  jCommander.usage();
			}
			finally
			{
				cmdStr = null;
			}
		}
		
		httpfsCmd.killAll();
	}
	
	private String[] splitCommandLineInput(String inputStr)
	{
		List<String> list = new ArrayList<String>();
		// split on \s and group terms in "" and ignore escaped \"
		Matcher m = Pattern.compile("(\\\"[^\\\"]*?(?:\\\\\\\"[^\\\"]*?)*\\\"|[^\\s]+)").matcher(inputStr);
		while (m.find())
		{
			String modifiedStr = m.group(1);
			
			if (modifiedStr.length() > 1 && modifiedStr.startsWith("\"") && modifiedStr.endsWith("\"") && !modifiedStr.endsWith("\\\""))
			{
				// remove leading and trailing "
				modifiedStr = modifiedStr.substring(1, modifiedStr.length()-1);
				
				// replace escaped \" with "
				modifiedStr = modifiedStr.replace("\\\"", "\"");
			}
		    
		    list.add(modifiedStr);
		}
		
		String[] splitStrArr = new String[list.size()];
		for (int i = 0; i < list.size(); ++i)
			splitStrArr[i] = list.get(i);
		
		return splitStrArr;
	}

	private void ResetAllCommands()
	{
		quitCmd.reset();
		httpfsCmd.reset();
		
		jCommander = JCommander.newBuilder()
				.addCommand(quitCmd)
				.addCommand(httpfsCmd)
				.build();
		
		jCommander.setUsageFormatter(new CommandUsageFormatter(jCommander));
		httpfsCmd.setCommander(jCommander);
	}
}
