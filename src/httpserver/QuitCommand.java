package httpserver;

import com.beust.jcommander.Parameters;

@Parameters(commandNames = "quit", commandDescription = "quit the program.")
public class QuitCommand implements ICommand
{
	public QuitCommand()
	{
		reset();
	}
	
	@Override
	public void reset()
	{
		//
	}
	
	@Override
	public void execute()
	{
		System.exit(0);
	}
}
